import React, { useEffect, useState } from "react";

import "./App.scss";
import { Home } from "./components";
import graphQlToRest from "./helpers/data";

function App() {
  const [cardData, setCardData] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    graphQlToRest()
      .then((cardData) =>
        setCardData(cardData.data.getHomePageArticle.aResults)
      )
      .then(() => setLoading(false));
  }, []);

  return (
    <div className="app">
      <div className="container">
        {loading ? <h1>Loading...</h1> : <Home cardData={cardData} />}
      </div>
    </div>
  );
}

export default App;
