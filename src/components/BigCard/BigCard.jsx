import React from "react";
import PropTypes from "prop-types";
import dateFormat from "dateformat";
import { BsFillCalendarEventFill, BsAlarmFill } from "react-icons/bs";

import "./BigCard.scss";
import { defaultImg } from "../../assets/images";

function BigCard({ article }) {
  function addDefaultSrc(e) {
    e.target.src = defaultImg;
  }

  const publishedDate = dateFormat(
    new Date(article.dPublishDate),
    "d mmm yyyy"
  );

  return (
    <div className="bigCard">
      <img onError={addDefaultSrc} src={article.oImg.sUrl} alt="BigCardImg" />
      <h3>{article.sTitle}</h3>
      <p>{article.sDescription}</p>

      <div className="date-container">
        <BsFillCalendarEventFill className="icon" />
        <p>{publishedDate}</p>
        <BsAlarmFill className="icon" />
        <p className="duration">{article.nDuration} Min</p>
      </div>
    </div>
  );
}

BigCard.propTypes = {
  article: PropTypes.object,
};

export default BigCard;
