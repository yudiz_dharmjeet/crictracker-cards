import React from "react";
import PropTypes from "prop-types";
import { IoMdArrowDropright } from "react-icons/io";

import "./Home.scss";
import Articles from "../Articles/Articles";
import Helmet from "react-helmet";

function Home({ cardData }) {
  return (
    <div className="home">
      <Helmet>
        <meta charSet="utf-8" />
        <meta
          name="CricTracker"
          content="This is basic articles display page"
        />
        <title>Cric-Tracker</title>
        <link rel="canonical" href="http://localhost:3000" />
      </Helmet>

      {cardData.map((card, index) => (
        <div className="card" key={index}>
          <h1>{card.sName}</h1>
          <hr className="hr" />
          <Articles articles={card.aArticle} />
          <button>
            More From {card.sName} <IoMdArrowDropright />
          </button>
        </div>
      ))}
    </div>
  );
}

Home.propTypes = {
  cardData: PropTypes.array,
};

export default Home;
