import React from "react";
import PropTypes from "prop-types";
import { BsFillCalendarEventFill, BsAlarmFill } from "react-icons/bs";
import dateFormat from "dateformat";

import "./SmallCard.scss";
import { defaultImg } from "../../assets/images";

function SmallCard({ article }) {
  function addDefaultSrc(e) {
    e.target.src = defaultImg;
  }

  const publishedDate = dateFormat(
    new Date(article.dPublishDate),
    "d mmm yyyy"
  );

  return (
    <div className="smallCard">
      <div className="smallCard__img-container">
        <img
          onError={addDefaultSrc}
          src={article.oImg.sUrl ? article.oImg.sUrl : defaultImg}
          alt="SmallCardImg"
        />
      </div>
      <div className="smallCard__desc">
        <h3>{article.sTitle}</h3>
        <div className="date-container">
          <BsFillCalendarEventFill className="icon" />
          <p>{publishedDate}</p>
          <BsAlarmFill className="icon" />
          <p className="duration">{article.nDuration} Min</p>
        </div>
      </div>
    </div>
  );
}

SmallCard.propTypes = {
  article: PropTypes.object,
};

export default SmallCard;
