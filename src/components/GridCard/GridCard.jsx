import React from "react";
import PropTypes from "prop-types";
import dateFormat from "dateformat";
import { BsFillCalendarEventFill, BsAlarmFill } from "react-icons/bs";

import "./GridCard.scss";
import { defaultImg } from "../../assets/images";

function GridCard({ article }) {
  function addDefaultSrc(e) {
    e.target.src = defaultImg;
  }

  const publishedDate = dateFormat(
    new Date(article.dPublishDate),
    "d mmm yyyy"
  );

  return (
    <div className="gridCard">
      <div className="gridCard__top">
        <img onError={addDefaultSrc} src={article.oImg.sUrl} alt="BigCardImg" />
        <h3>{article.sTitle}</h3>
      </div>

      <div className="date-container">
        <BsFillCalendarEventFill className="icon" />
        <p>{publishedDate}</p>
        <BsAlarmFill className="icon" />
        <p className="duration">{article.nDuration} Min</p>
      </div>
    </div>
  );
}

GridCard.propTypes = {
  article: PropTypes.object,
};

export default GridCard;
