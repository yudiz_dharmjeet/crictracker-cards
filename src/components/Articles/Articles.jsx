import React from "react";
import PropTypes from "prop-types";

import "./Articles.scss";
import BigCard from "../BigCard/BigCard";
import SmallCard from "../SmallCard/SmallCard";
import GridCard from "../GridCard/GridCard";

function Articles({ articles }) {
  return (
    <div className="articles">
      {articles.map((article, index) => (
        <React.Fragment key={index}>
          {article.sType == "nBig" && <BigCard article={article} />}
          {article.sType == "nSmall" && <SmallCard article={article} />}
        </React.Fragment>
      ))}

      <div className="gridCards">
        {articles.map((article, index) => (
          <React.Fragment key={index}>
            {article.sType == "nGrid" && <GridCard article={article} />}
          </React.Fragment>
        ))}
      </div>
    </div>
  );
}

Articles.propTypes = {
  articles: PropTypes.array,
};

export default Articles;
