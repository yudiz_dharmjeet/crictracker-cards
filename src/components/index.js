import Home from "./Home/Home";
import Articles from "./Articles/Articles";
import BigCard from "./BigCard/BigCard";
import SmallCard from "./SmallCard/SmallCard";
import GridCard from "./GridCard/GridCard";

export { Home, Articles, BigCard, SmallCard, GridCard };
